<?php

class AppModel extends Model {
	function beforeSave(){
		$this->timeStamper();
		$this->timeStamper('Updated');
		return true; //necessary to complete the request
	}	
	
	function timeStamper($label = 'Created'){
		$model = $this->name;
		if($created_type = $this->_schema[$label]['type']):
			if($created_type == "date"):
				$stamp = date('Y-m-d');
//debug($stamp);
			elseif($created_type == "timestamp"):
				$stamp = date('Y-m-d H:i:s');
//debug($stamp);				
			else:
				$stamp = "";
//debug($this->_schema);				
			endif;
			
			if($label == 'Created' && @$this->data[$model][$label]!=""):
				//do nada because Created value should be preserved if NOT empty
			
			else:
				$this->data[$model][$label]=$stamp;
			endif;
		else:
			//shouldn't happen
		endif;
			
	}
}
