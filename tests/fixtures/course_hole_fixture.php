<?php
/* CourseHole Fixture generated on: 2013-06-08 18:56:28 : 1370732188 */
class CourseHoleFixture extends CakeTestFixture {
	var $name = 'CourseHole';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'course_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'Active' => array('type' => 'string', 'null' => false, 'default' => '1', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Color' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 25, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'HoleNumber' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 4),
		'Par' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 4),
		'Yardage' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 6),
		'Created' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'Updated' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'course_id' => 1,
			'Active' => 'Lorem ipsum dolor sit ame',
			'Color' => 'Lorem ipsum dolor sit a',
			'HoleNumber' => 1,
			'Par' => 1,
			'Yardage' => 1,
			'Created' => '2013-06-08',
			'Updated' => 1370732188
		),
	);
}
