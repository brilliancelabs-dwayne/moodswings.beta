<?php
/* Scorecard Fixture generated on: 2013-06-08 18:56:29 : 1370732189 */
class ScorecardFixture extends CakeTestFixture {
	var $name = 'Scorecard';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'round_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'moodswing_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'Active' => array('type' => 'string', 'null' => false, 'default' => '1', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Created' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'Updated' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'DynKey' => array('column' => array('round_id', 'moodswing_id'), 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'round_id' => 1,
			'moodswing_id' => 1,
			'Active' => 'Lorem ipsum dolor sit ame',
			'Created' => '2013-06-08',
			'Updated' => 1370732189
		),
	);
}
