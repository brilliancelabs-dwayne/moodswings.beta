<?php
/* Round Test cases generated on: 2013-06-08 18:56:29 : 1370732189*/
App::import('Model', 'Round');

class RoundTestCase extends CakeTestCase {
	var $fixtures = array('app.round', 'app.device', 'app.club', 'app.club_type', 'app.course', 'app.course_hole', 'app.moodswing', 'app.scorecard', 'app.user');

	function startTest() {
		$this->Round =& ClassRegistry::init('Round');
	}

	function endTest() {
		unset($this->Round);
		ClassRegistry::flush();
	}

}
