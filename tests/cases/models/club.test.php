<?php
/* Club Test cases generated on: 2013-06-08 18:56:28 : 1370732188*/
App::import('Model', 'Club');

class ClubTestCase extends CakeTestCase {
	var $fixtures = array('app.club', 'app.device', 'app.course', 'app.course_hole', 'app.moodswing', 'app.round', 'app.scorecard', 'app.user', 'app.club_type');

	function startTest() {
		$this->Club =& ClassRegistry::init('Club');
	}

	function endTest() {
		unset($this->Club);
		ClassRegistry::flush();
	}

}
