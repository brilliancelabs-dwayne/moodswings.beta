<?php
/* Course Test cases generated on: 2013-06-08 18:56:28 : 1370732188*/
App::import('Model', 'Course');

class CourseTestCase extends CakeTestCase {
	var $fixtures = array('app.course', 'app.device', 'app.club', 'app.club_type', 'app.round', 'app.moodswing', 'app.course_hole', 'app.scorecard', 'app.user');

	function startTest() {
		$this->Course =& ClassRegistry::init('Course');
	}

	function endTest() {
		unset($this->Course);
		ClassRegistry::flush();
	}

}
