<?php
/* Scorecard Test cases generated on: 2013-06-08 18:56:29 : 1370732189*/
App::import('Model', 'Scorecard');

class ScorecardTestCase extends CakeTestCase {
	var $fixtures = array('app.scorecard', 'app.round', 'app.device', 'app.club', 'app.club_type', 'app.course', 'app.course_hole', 'app.moodswing', 'app.user');

	function startTest() {
		$this->Scorecard =& ClassRegistry::init('Scorecard');
	}

	function endTest() {
		unset($this->Scorecard);
		ClassRegistry::flush();
	}

}
