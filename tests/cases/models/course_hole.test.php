<?php
/* CourseHole Test cases generated on: 2013-06-08 18:56:28 : 1370732188*/
App::import('Model', 'CourseHole');

class CourseHoleTestCase extends CakeTestCase {
	var $fixtures = array('app.course_hole', 'app.course', 'app.device', 'app.club', 'app.club_type', 'app.round', 'app.moodswing', 'app.scorecard', 'app.user');

	function startTest() {
		$this->CourseHole =& ClassRegistry::init('CourseHole');
	}

	function endTest() {
		unset($this->CourseHole);
		ClassRegistry::flush();
	}

}
