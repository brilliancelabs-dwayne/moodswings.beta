<?php
/* ClubType Test cases generated on: 2013-06-08 18:56:28 : 1370732188*/
App::import('Model', 'ClubType');

class ClubTypeTestCase extends CakeTestCase {
	var $fixtures = array('app.club_type', 'app.club', 'app.device', 'app.course', 'app.course_hole', 'app.moodswing', 'app.round', 'app.scorecard', 'app.user');

	function startTest() {
		$this->ClubType =& ClassRegistry::init('ClubType');
	}

	function endTest() {
		unset($this->ClubType);
		ClassRegistry::flush();
	}

}
