<?php
/* User Test cases generated on: 2013-06-08 18:56:30 : 1370732190*/
App::import('Model', 'User');

class UserTestCase extends CakeTestCase {
	var $fixtures = array('app.user', 'app.device', 'app.club', 'app.club_type', 'app.course', 'app.course_hole', 'app.moodswing', 'app.round', 'app.scorecard');

	function startTest() {
		$this->User =& ClassRegistry::init('User');
	}

	function endTest() {
		unset($this->User);
		ClassRegistry::flush();
	}

}
