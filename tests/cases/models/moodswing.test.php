<?php
/* Moodswing Test cases generated on: 2013-06-08 18:56:29 : 1370732189*/
App::import('Model', 'Moodswing');

class MoodswingTestCase extends CakeTestCase {
	var $fixtures = array('app.moodswing', 'app.round', 'app.device', 'app.club', 'app.club_type', 'app.course', 'app.course_hole', 'app.user', 'app.scorecard');

	function startTest() {
		$this->Moodswing =& ClassRegistry::init('Moodswing');
	}

	function endTest() {
		unset($this->Moodswing);
		ClassRegistry::flush();
	}

}
