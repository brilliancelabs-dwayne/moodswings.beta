<?php
/* Device Test cases generated on: 2013-06-08 18:56:29 : 1370732189*/
App::import('Model', 'Device');

class DeviceTestCase extends CakeTestCase {
	var $fixtures = array('app.device', 'app.club', 'app.club_type', 'app.course', 'app.course_hole', 'app.moodswing', 'app.round', 'app.scorecard', 'app.user');

	function startTest() {
		$this->Device =& ClassRegistry::init('Device');
	}

	function endTest() {
		unset($this->Device);
		ClassRegistry::flush();
	}

}
