<?php
/* Scorecards Test cases generated on: 2013-06-08 18:56:30 : 1370732190*/
App::import('Controller', 'Scorecards');

class TestScorecardsController extends ScorecardsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ScorecardsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.scorecard', 'app.round', 'app.device', 'app.club', 'app.club_type', 'app.moodswing', 'app.course', 'app.course_hole', 'app.user');

	function startTest() {
		$this->Scorecards =& new TestScorecardsController();
		$this->Scorecards->constructClasses();
	}

	function endTest() {
		unset($this->Scorecards);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
