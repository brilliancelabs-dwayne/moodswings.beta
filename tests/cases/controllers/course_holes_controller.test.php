<?php
/* CourseHoles Test cases generated on: 2013-06-08 18:56:30 : 1370732190*/
App::import('Controller', 'CourseHoles');

class TestCourseHolesController extends CourseHolesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class CourseHolesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.course_hole', 'app.course', 'app.device', 'app.club', 'app.club_type', 'app.moodswing', 'app.round', 'app.scorecard', 'app.user');

	function startTest() {
		$this->CourseHoles =& new TestCourseHolesController();
		$this->CourseHoles->constructClasses();
	}

	function endTest() {
		unset($this->CourseHoles);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
