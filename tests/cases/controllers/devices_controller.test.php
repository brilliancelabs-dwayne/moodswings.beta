<?php
/* Devices Test cases generated on: 2013-06-08 18:56:30 : 1370732190*/
App::import('Controller', 'Devices');

class TestDevicesController extends DevicesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class DevicesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.device', 'app.club', 'app.club_type', 'app.moodswing', 'app.round', 'app.course', 'app.course_hole', 'app.scorecard', 'app.user');

	function startTest() {
		$this->Devices =& new TestDevicesController();
		$this->Devices->constructClasses();
	}

	function endTest() {
		unset($this->Devices);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
