<?php
/* Moodswings Test cases generated on: 2013-06-08 18:56:30 : 1370732190*/
App::import('Controller', 'Moodswings');

class TestMoodswingsController extends MoodswingsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MoodswingsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.moodswing', 'app.round', 'app.device', 'app.club', 'app.club_type', 'app.course', 'app.course_hole', 'app.user', 'app.scorecard');

	function startTest() {
		$this->Moodswings =& new TestMoodswingsController();
		$this->Moodswings->constructClasses();
	}

	function endTest() {
		unset($this->Moodswings);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
