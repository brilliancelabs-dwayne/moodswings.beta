<?php
/* Clubs Test cases generated on: 2013-06-08 18:56:30 : 1370732190*/
App::import('Controller', 'Clubs');

class TestClubsController extends ClubsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ClubsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.club', 'app.device', 'app.course', 'app.course_hole', 'app.round', 'app.moodswing', 'app.scorecard', 'app.user', 'app.club_type');

	function startTest() {
		$this->Clubs =& new TestClubsController();
		$this->Clubs->constructClasses();
	}

	function endTest() {
		unset($this->Clubs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
