<?php
/* ClubTypes Test cases generated on: 2013-06-08 18:56:30 : 1370732190*/
App::import('Controller', 'ClubTypes');

class TestClubTypesController extends ClubTypesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ClubTypesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.club_type', 'app.club', 'app.device', 'app.course', 'app.course_hole', 'app.round', 'app.moodswing', 'app.scorecard', 'app.user');

	function startTest() {
		$this->ClubTypes =& new TestClubTypesController();
		$this->ClubTypes->constructClasses();
	}

	function endTest() {
		unset($this->ClubTypes);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
