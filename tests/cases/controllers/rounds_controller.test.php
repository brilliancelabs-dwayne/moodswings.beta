<?php
/* Rounds Test cases generated on: 2013-06-08 18:56:30 : 1370732190*/
App::import('Controller', 'Rounds');

class TestRoundsController extends RoundsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class RoundsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.round', 'app.device', 'app.club', 'app.club_type', 'app.moodswing', 'app.scorecard', 'app.course', 'app.course_hole', 'app.user');

	function startTest() {
		$this->Rounds =& new TestRoundsController();
		$this->Rounds->constructClasses();
	}

	function endTest() {
		unset($this->Rounds);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
