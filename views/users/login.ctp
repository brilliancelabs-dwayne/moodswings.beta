<div class="users form">
<?php echo $this->Form->create('User');?>
	<fieldset>
		<legend><?php __('Login'); ?></legend>
	<?php
		echo $this->Form->input('username');
		echo $this->Form->input('password');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Login', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
	<?php if(isset($show_people_features)):?>
		<li><?php echo $this->Html->link(__('New Person', true), array('controller' => 'people', 'action' => 'add'));?></li>
	<?php endif;?>
		<li><?php echo $this->Html->link(__('Register User', true), array('action' => 'register'));?></li>
	</ul>
</div>
