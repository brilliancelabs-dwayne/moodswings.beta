<div class="clubs form">
<?php echo $this->Form->create('Club');?>
	<fieldset>
		<legend><?php __('Edit Club'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('device_id');
		echo $this->Form->input('Name');
		echo $this->Form->input('club_type_id');
		echo $this->Form->input('Description');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Club.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Club.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Clubs', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Devices', true), array('controller' => 'devices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Device', true), array('controller' => 'devices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Club Types', true), array('controller' => 'club_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Club Type', true), array('controller' => 'club_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Moodswings', true), array('controller' => 'moodswings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Moodswing', true), array('controller' => 'moodswings', 'action' => 'add')); ?> </li>
	</ul>
</div>