<div class="clubs index">
	<h2><?php __('Clubs');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('device_id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('club_type_id');?></th>
			<th><?php echo $this->Paginator->sort('Description');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($clubs as $club):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $club['Club']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($club['Device']['id'], array('controller' => 'devices', 'action' => 'view', $club['Device']['id'])); ?>
		</td>
		<td><?php echo $club['Club']['Name']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($club['ClubType']['id'], array('controller' => 'club_types', 'action' => 'view', $club['ClubType']['id'])); ?>
		</td>
		<td><?php echo $club['Club']['Description']; ?>&nbsp;</td>
		<td><?php echo $club['Club']['Active']; ?>&nbsp;</td>
		<td><?php echo $club['Club']['Created']; ?>&nbsp;</td>
		<td><?php echo $club['Club']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $club['Club']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $club['Club']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $club['Club']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $club['Club']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Club', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Devices', true), array('controller' => 'devices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Device', true), array('controller' => 'devices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Club Types', true), array('controller' => 'club_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Club Type', true), array('controller' => 'club_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Moodswings', true), array('controller' => 'moodswings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Moodswing', true), array('controller' => 'moodswings', 'action' => 'add')); ?> </li>
	</ul>
</div>