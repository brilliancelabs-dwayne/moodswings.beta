<div class="clubs view">
<h2><?php  __('Club');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $club['Club']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Device'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($club['Device']['id'], array('controller' => 'devices', 'action' => 'view', $club['Device']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $club['Club']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Club Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($club['ClubType']['id'], array('controller' => 'club_types', 'action' => 'view', $club['ClubType']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $club['Club']['Description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $club['Club']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $club['Club']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $club['Club']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Club', true), array('action' => 'edit', $club['Club']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Club', true), array('action' => 'delete', $club['Club']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $club['Club']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Clubs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Club', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Devices', true), array('controller' => 'devices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Device', true), array('controller' => 'devices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Club Types', true), array('controller' => 'club_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Club Type', true), array('controller' => 'club_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Moodswings', true), array('controller' => 'moodswings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Moodswing', true), array('controller' => 'moodswings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Moodswings');?></h3>
	<?php if (!empty($club['Moodswing'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Round Id'); ?></th>
		<th><?php __('Club Id'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('HoleNumber'); ?></th>
		<th><?php __('Stroke'); ?></th>
		<th><?php __('Mood'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($club['Moodswing'] as $moodswing):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $moodswing['id'];?></td>
			<td><?php echo $moodswing['round_id'];?></td>
			<td><?php echo $moodswing['club_id'];?></td>
			<td><?php echo $moodswing['Active'];?></td>
			<td><?php echo $moodswing['HoleNumber'];?></td>
			<td><?php echo $moodswing['Stroke'];?></td>
			<td><?php echo $moodswing['Mood'];?></td>
			<td><?php echo $moodswing['Created'];?></td>
			<td><?php echo $moodswing['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'moodswings', 'action' => 'view', $moodswing['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'moodswings', 'action' => 'edit', $moodswing['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'moodswings', 'action' => 'delete', $moodswing['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $moodswing['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Moodswing', true), array('controller' => 'moodswings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
