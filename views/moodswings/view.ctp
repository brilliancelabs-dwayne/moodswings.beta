<div class="moodswings view">
<h2><?php  __('Moodswing');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $moodswing['Moodswing']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Round'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($moodswing['Round']['id'], array('controller' => 'rounds', 'action' => 'view', $moodswing['Round']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Club'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($moodswing['Club']['id'], array('controller' => 'clubs', 'action' => 'view', $moodswing['Club']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $moodswing['Moodswing']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('HoleNumber'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $moodswing['Moodswing']['HoleNumber']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Stroke'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $moodswing['Moodswing']['Stroke']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Mood'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $moodswing['Moodswing']['Mood']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $moodswing['Moodswing']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $moodswing['Moodswing']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Moodswing', true), array('action' => 'edit', $moodswing['Moodswing']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Moodswing', true), array('action' => 'delete', $moodswing['Moodswing']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $moodswing['Moodswing']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Moodswings', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Moodswing', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rounds', true), array('controller' => 'rounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round', true), array('controller' => 'rounds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clubs', true), array('controller' => 'clubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Club', true), array('controller' => 'clubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Scorecards', true), array('controller' => 'scorecards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scorecard', true), array('controller' => 'scorecards', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Scorecards');?></h3>
	<?php if (!empty($moodswing['Scorecard'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Round Id'); ?></th>
		<th><?php __('Moodswing Id'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($moodswing['Scorecard'] as $scorecard):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $scorecard['id'];?></td>
			<td><?php echo $scorecard['round_id'];?></td>
			<td><?php echo $scorecard['moodswing_id'];?></td>
			<td><?php echo $scorecard['Active'];?></td>
			<td><?php echo $scorecard['Created'];?></td>
			<td><?php echo $scorecard['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'scorecards', 'action' => 'view', $scorecard['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'scorecards', 'action' => 'edit', $scorecard['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'scorecards', 'action' => 'delete', $scorecard['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $scorecard['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Scorecard', true), array('controller' => 'scorecards', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
