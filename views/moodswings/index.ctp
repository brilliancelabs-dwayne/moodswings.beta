<div class="moodswings index">
	<h2><?php __('Moodswings');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('round_id');?></th>
			<th><?php echo $this->Paginator->sort('club_id');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('HoleNumber');?></th>
			<th><?php echo $this->Paginator->sort('Stroke');?></th>
			<th><?php echo $this->Paginator->sort('Mood');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($moodswings as $moodswing):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $moodswing['Moodswing']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($moodswing['Round']['id'], array('controller' => 'rounds', 'action' => 'view', $moodswing['Round']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($moodswing['Club']['id'], array('controller' => 'clubs', 'action' => 'view', $moodswing['Club']['id'])); ?>
		</td>
		<td><?php echo $moodswing['Moodswing']['Active']; ?>&nbsp;</td>
		<td><?php echo $moodswing['Moodswing']['HoleNumber']; ?>&nbsp;</td>
		<td><?php echo $moodswing['Moodswing']['Stroke']; ?>&nbsp;</td>
		<td><?php echo $moodswing['Moodswing']['Mood']; ?>&nbsp;</td>
		<td><?php echo $moodswing['Moodswing']['Created']; ?>&nbsp;</td>
		<td><?php echo $moodswing['Moodswing']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $moodswing['Moodswing']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $moodswing['Moodswing']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $moodswing['Moodswing']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $moodswing['Moodswing']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Moodswing', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Rounds', true), array('controller' => 'rounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round', true), array('controller' => 'rounds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clubs', true), array('controller' => 'clubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Club', true), array('controller' => 'clubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Scorecards', true), array('controller' => 'scorecards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scorecard', true), array('controller' => 'scorecards', 'action' => 'add')); ?> </li>
	</ul>
</div>