<div class="moodswings form">
<?php echo $this->Form->create('Moodswing');?>
	<fieldset>
		<legend><?php __('Edit Moodswing'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('round_id');
		echo $this->Form->input('club_id');
		echo $this->Form->input('Active');
		echo $this->Form->input('HoleNumber');
		echo $this->Form->input('Stroke');
		echo $this->Form->input('Mood');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Moodswing.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Moodswing.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Moodswings', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Rounds', true), array('controller' => 'rounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round', true), array('controller' => 'rounds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clubs', true), array('controller' => 'clubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Club', true), array('controller' => 'clubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Scorecards', true), array('controller' => 'scorecards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scorecard', true), array('controller' => 'scorecards', 'action' => 'add')); ?> </li>
	</ul>
</div>