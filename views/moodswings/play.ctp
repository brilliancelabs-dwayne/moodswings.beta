<?php 
echo $this->Html->script('PLAY');
echo $this->Html->css('play');
?>
<div class="index">
<h2>Play</h2>
<?php 
$this_course = $this->Session->read('Play.Course.id');
if(!@$this_course):?>
<?php 

echo $this->Form->create();
echo $this->Form->input('Course', array('options' => @$courses));
echo $this->Form->input('StartHole', array('value' => 1));
echo $this->Form->submit('Load');
echo $this->Form->end();
?>
<?php else:
$golfer = $this->Session->read('Auth.User.username');
$hole = $this->Session->read('Play.Current.Hole');
$stroke = $this->Session->read('Play.Current.Stroke');
$temp = $this->Session->read('Play.Course.Temperature');
#debug($this->Session->read('Play'));
?>

<?php echo $this->Form->create();?>
<?php 
//Render the play grid if the mood grid is not requested
if(!isset($get_mood)):
?>
	<table id="play-grid">
		<tr>
			<th><h3><?php echo $golfer;?></h3></th>
			<th><?php echo $temp;?></th>
		</tr>			
		<tr>
			<td>Hole</td>
			<td><?php echo $hole?></td>
		</tr>
		<tr>
			<td>Stroke</td>
			<td><?php echo $stroke;?></td>
		</tr>	
	
		<tr>
			<td>Club</td>
			<td><?php echo $this->Form->input('Club', array('options' => @$clubs, 'label' => false, 'div' => false));?></td>
		</tr>
		
	</table>
<?php echo $this->Form->submit('Save');?>

<?php else:?>
	<table id="mood-grid">	
		<tr><th colspan="4">How did that feel?</th></tr>
		<tr>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
		</tr>
		
	</table>
<?php endif;?>
<?php echo $this->Form->end();?>
<?php endif;?>
</div>