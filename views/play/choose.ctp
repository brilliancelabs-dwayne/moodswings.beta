<div class="index">
	<h2>Choose Course</h2>
	<div class="actions">
		<ul>
			<?php foreach($courses as $id => $name):?>
			<li><?php echo $this->Html->link($name, 'choose/'.$id);?></li>
			<?php endforeach;?>
		</ul>
	</div>
</div>