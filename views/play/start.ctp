<div class="index">
	<h2>Choose a Starting Hole</h2>
	<div class="actions">
		<ul>
			<?php for($i=1;$i<=18;$i++):?>
			<li><?php echo $this->Html->link($i, 'start/'.$i);?></li>
			<?php endfor;?>
		</ul>
	</div>
</div>