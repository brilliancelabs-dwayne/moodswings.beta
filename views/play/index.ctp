<?php 
echo $this->Html->css('play');
echo $this->Html->script('PLAY');
?>

<?php 
//Make URL available in the elements
$url =@ "/".$this->params['url']['url']."/";
$this->set('url', $url);
?>
<div class="index">
<div id="title-bar">
	<h2>Play <span class="clear-data"><a href="/exec/clear/Play">Clear Data</a> | <a href="/play/endgame">End Game</a></span></h2>
	<span class="right">
		<span id="hole-title">Hole <?php echo @$hole;?> </span>
		<span id="stroke-title">Stroke <?php echo @$stroke?></span>
	</span>
</div>

<?php 
if(@$mode):
	echo $this->element('/modes/'.@$mode);
endif;
?>

<?php if(!empty($shot_log)):?>
<div class="shot_log">
<ul>
<h4>Hole <?php echo $hole?> Shot Log
<?php if($this->Session->check('Play.Round')):?>
<span class="hole_strip">
<?php foreach($this->Session->read('Play.Round') as $r => $pr):?>
	<span><a href="<?php echo $r?>"><?php echo $r?></a></span>
<?php endforeach;?>
</span>
<?php endif;?>

</h4>

<?php 
$istroke = 1;
foreach($shot_log as $sl_stroke => $cm):
	//set mood image
	$sl_club = array_keys($cm);
	$sl_club = @$sl_club[0];
	$sl_mood = @$cm[$sl_club];

	$mood_image = $this->Html->image('icons/moods/'.$sl_mood);
?>
	<li class="logline"><?php echo $this->Html->link(@$mood_image, array(
				'controller' => 'play',
				'action' => 'round', "/".$hole."/".$sl_stroke."/".$sl_club
			), array('escape' => false));?>
	<div><?php echo $clubs[$sl_club]['Name']?>
	</div>
	</li>
<?php 
	$istroke++;
endforeach;?>
</ul>
</div>
<?php endif;?>


</div>
