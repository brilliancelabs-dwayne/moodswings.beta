<?php 
echo $this->Html->css('play');
echo $this->Html->script('PLAY');
?>

<?php 
//Make URL available in the elements
$url =@ "/".$this->params['url']['url']."/";
$this->set('url', $url);
?>
<div class="index">
<h2>Game Tracking Over</h2>
<h3>Thank you for using Mood Swings!!</h3>


<br />
<br /><br />
<br /><br />
<br />
<div>
<h4>Round Data in a Code Debug</h4>
<br />
<h2> > Hole Number (outer element)</h2>
<br />
<h2> >> Hole Stroke Number (1st level)</h2>
<br />
<h2> >>> Club => Mood Score (2nd level)</h2>
<br />
<br />
</div>
<?php 
debug($this->Session->read('Play.Round'));
?>
</div>