<div class="courses form">
<?php echo $this->Form->create('Course');?>
	<fieldset>
		<legend><?php __('Edit Course'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('device_id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Description');
		echo $this->Form->input('Active');
		echo $this->Form->input('SubCourseName');
		echo $this->Form->input('Slope');
		echo $this->Form->input('Rating');
		echo $this->Form->input('ApiCourseId');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Course.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Course.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Courses', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Devices', true), array('controller' => 'devices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Device', true), array('controller' => 'devices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Course Holes', true), array('controller' => 'course_holes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Course Hole', true), array('controller' => 'course_holes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rounds', true), array('controller' => 'rounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round', true), array('controller' => 'rounds', 'action' => 'add')); ?> </li>
	</ul>
</div>