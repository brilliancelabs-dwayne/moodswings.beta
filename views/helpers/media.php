<?php
class MediaHelper extends AppHelper {
   
	public function youtube($code = null, $w = 560, $h = 315){
      /**Credit for z-index issue fix:
       * http://stackoverflow.com/questions/9074365/youtube-video-embedded-via-iframe-ignoring-z-index
       */
		$codeblock = '
		<iframe 
			width="%d" height="%d" 
			src="https://www.youtube-nocookie.com/embed/%s?rel=0&wmode=transparent" 
			frameborder="0" allowfullscreen wmode="Opaque">
		</iframe>';
		
		if($code && $w && $h):
			$video = sprintf($codeblock, $w, $h, $code);
			return $video;
		endif;
		
	
	}
	
   public function audio($audio = array(), $options = 'controls no-autoplay'){

   		if(isset($audio['file']) && isset($audio['filetype'])):
	   		$code='<audio %s>'. "\n\r";
	   		$code.='<source src="%s" type="%s">'. "\n\r";
	   		$code.='Your browser does not support the audio element.'. "\n\r";
	   		$code.='</audio>'. "\n\r";
	   		
	   		printf($code,$options, $audio['file'], $audio['filetype']);
	   		
   		endif;
   }

}
?>
