<div class="courseHoles index">
	<h2><?php __('Course Holes');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('course_id');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Color');?></th>
			<th><?php echo $this->Paginator->sort('HoleNumber');?></th>
			<th><?php echo $this->Paginator->sort('Par');?></th>
			<th><?php echo $this->Paginator->sort('Yardage');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($courseHoles as $courseHole):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $courseHole['CourseHole']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($courseHole['Course']['id'], array('controller' => 'courses', 'action' => 'view', $courseHole['Course']['id'])); ?>
		</td>
		<td><?php echo $courseHole['CourseHole']['Active']; ?>&nbsp;</td>
		<td><?php echo $courseHole['CourseHole']['Color']; ?>&nbsp;</td>
		<td><?php echo $courseHole['CourseHole']['HoleNumber']; ?>&nbsp;</td>
		<td><?php echo $courseHole['CourseHole']['Par']; ?>&nbsp;</td>
		<td><?php echo $courseHole['CourseHole']['Yardage']; ?>&nbsp;</td>
		<td><?php echo $courseHole['CourseHole']['Created']; ?>&nbsp;</td>
		<td><?php echo $courseHole['CourseHole']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $courseHole['CourseHole']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $courseHole['CourseHole']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $courseHole['CourseHole']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $courseHole['CourseHole']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Course Hole', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Courses', true), array('controller' => 'courses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Course', true), array('controller' => 'courses', 'action' => 'add')); ?> </li>
	</ul>
</div>