<div class="courseHoles form">
<?php echo $this->Form->create('CourseHole');?>
	<fieldset>
		<legend><?php __('Add Course Hole'); ?></legend>
	<?php
		echo $this->Form->input('course_id');
		echo $this->Form->input('Active');
		echo $this->Form->input('Color');
		echo $this->Form->input('HoleNumber');
		echo $this->Form->input('Par');
		echo $this->Form->input('Yardage');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Course Holes', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Courses', true), array('controller' => 'courses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Course', true), array('controller' => 'courses', 'action' => 'add')); ?> </li>
	</ul>
</div>