<?php if(!isset($AuthStatus)): $AuthStatus = 0;endif;?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php __($appName.' | Powered by the Application Construct'); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(array(
			'reset', //typical reset
			'blac2', //from the BL app construct
			'/js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom',
			'default', //different for each application
		));

		echo $this->Html->script(array(
			#'http://www.brilliancelabs.com/jquery.js', 
			'jquery-latest',			
			#'http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js',
			'jquery-ui/js/jquery-ui-1.9.2.custom.js',
			'default',
			'UIFX',
		));
?>
		<script>
		 $(function() {
			//Make all datepicker classes active
			$(".datepicker" ).datepicker();

			//set app base
			window.appbase = '<?php echo @$this->base;?>';
		 });
		 </script>
	<?php
		echo $scripts_for_layout;
	?>
</head>
<body>
	<div id="container">
		<div id="header">
			<h2 id="logo"><?php echo @$appName;?></h2>
			<h3 id="tagline" class="rounded"><?php echo @$tagLine;?></h3>
			<div class="status">
			<?php if($AuthStatus):?>
				<?php if($user):?>
					<div>
						<?php echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'), array('class' => 'rounded'));?>
					</div>
					<div id="displayName">
						<?php echo $user['username'];?>
					</div>
					

				<?php else:?>
					<div class="stat_btn">
						<?php echo $this->Html->link('Register', array('controller' => 'users', 'action' => 'register'), array('class' => 'rounded'));?>
					</div> 
					<div class="stat_btn">
						<?php echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login'), array('class' => 'rounded'));?>
					</div>
				<?php endif;?>
			<?php endif;?>
			</div>
		</div>
		<div id="content">
<?php //debug($this);?>

			<?php 
			echo $this->Session->flash(); 
			?>

			<?php echo $content_for_layout; ?>

		</div>
		<!--  
		<div id="footer">
			<?php echo $this->element('sql_dump'); ?>
		</div>
		-->
	</div>
</body>
</html>
