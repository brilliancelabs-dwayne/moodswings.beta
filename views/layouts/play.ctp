<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>TalkGolf Beta
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(array(
			'play',
		));

		echo $this->Html->script(array(
			'jquery-latest',			
			'PLAY',
		));
	?>
		<script>
		 $(function() {
			
			//set app base
			window.appbase = "<?php echo @$this->base;?>";
	
			console.log(window.appbase);
		});
		</script>
	<?php echo $scripts_for_layout;?>
</head>
<body>
	<div id="container">
		<div id="header">
		</div>
		<div id="content">
			<?php 
			echo $this->Session->flash(); 
			echo $content_for_layout; 
			?>
		</div>
		<div id="footer">
		</div>
	</div>
</body>
</html>
