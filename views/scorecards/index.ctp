<div class="scorecards index">
	<h2><?php __('Scorecards');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('round_id');?></th>
			<th><?php echo $this->Paginator->sort('moodswing_id');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($scorecards as $scorecard):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $scorecard['Scorecard']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($scorecard['Round']['id'], array('controller' => 'rounds', 'action' => 'view', $scorecard['Round']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($scorecard['Moodswing']['id'], array('controller' => 'moodswings', 'action' => 'view', $scorecard['Moodswing']['id'])); ?>
		</td>
		<td><?php echo $scorecard['Scorecard']['Active']; ?>&nbsp;</td>
		<td><?php echo $scorecard['Scorecard']['Created']; ?>&nbsp;</td>
		<td><?php echo $scorecard['Scorecard']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $scorecard['Scorecard']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $scorecard['Scorecard']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $scorecard['Scorecard']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $scorecard['Scorecard']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Scorecard', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Rounds', true), array('controller' => 'rounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round', true), array('controller' => 'rounds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Moodswings', true), array('controller' => 'moodswings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Moodswing', true), array('controller' => 'moodswings', 'action' => 'add')); ?> </li>
	</ul>
</div>