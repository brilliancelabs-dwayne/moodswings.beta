<div class="scorecards form">
<?php echo $this->Form->create('Scorecard');?>
	<fieldset>
		<legend><?php __('Add Scorecard'); ?></legend>
	<?php
		echo $this->Form->input('round_id');
		echo $this->Form->input('moodswing_id');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Scorecards', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Rounds', true), array('controller' => 'rounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round', true), array('controller' => 'rounds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Moodswings', true), array('controller' => 'moodswings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Moodswing', true), array('controller' => 'moodswings', 'action' => 'add')); ?> </li>
	</ul>
</div>