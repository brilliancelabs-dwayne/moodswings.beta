<div class="clubTypes index">
	<h2><?php __('Club Types');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Description');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($clubTypes as $clubType):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $clubType['ClubType']['id']; ?>&nbsp;</td>
		<td><?php echo $clubType['ClubType']['Name']; ?>&nbsp;</td>
		<td><?php echo $clubType['ClubType']['Description']; ?>&nbsp;</td>
		<td><?php echo $clubType['ClubType']['Active']; ?>&nbsp;</td>
		<td><?php echo $clubType['ClubType']['Created']; ?>&nbsp;</td>
		<td><?php echo $clubType['ClubType']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $clubType['ClubType']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $clubType['ClubType']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $clubType['ClubType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $clubType['ClubType']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Club Type', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Clubs', true), array('controller' => 'clubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Club', true), array('controller' => 'clubs', 'action' => 'add')); ?> </li>
	</ul>
</div>