<div class="clubTypes form">
<?php echo $this->Form->create('ClubType');?>
	<fieldset>
		<legend><?php __('Edit Club Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Description');
		echo $this->Form->input('Active');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ClubType.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ClubType.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Club Types', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Clubs', true), array('controller' => 'clubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Club', true), array('controller' => 'clubs', 'action' => 'add')); ?> </li>
	</ul>
</div>