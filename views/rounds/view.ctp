<div class="rounds view">
<h2><?php  __('Round');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $round['Round']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Device'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($round['Device']['id'], array('controller' => 'devices', 'action' => 'view', $round['Device']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Course'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($round['Course']['id'], array('controller' => 'courses', 'action' => 'view', $round['Course']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $round['Round']['Active']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $round['Round']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $round['Round']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Round', true), array('action' => 'edit', $round['Round']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Round', true), array('action' => 'delete', $round['Round']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $round['Round']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Rounds', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Round', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Devices', true), array('controller' => 'devices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Device', true), array('controller' => 'devices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Courses', true), array('controller' => 'courses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Course', true), array('controller' => 'courses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Moodswings', true), array('controller' => 'moodswings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Moodswing', true), array('controller' => 'moodswings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Scorecards', true), array('controller' => 'scorecards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scorecard', true), array('controller' => 'scorecards', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Moodswings');?></h3>
	<?php if (!empty($round['Moodswing'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Round Id'); ?></th>
		<th><?php __('Club Id'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('HoleNumber'); ?></th>
		<th><?php __('Stroke'); ?></th>
		<th><?php __('Mood'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($round['Moodswing'] as $moodswing):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $moodswing['id'];?></td>
			<td><?php echo $moodswing['round_id'];?></td>
			<td><?php echo $moodswing['club_id'];?></td>
			<td><?php echo $moodswing['Active'];?></td>
			<td><?php echo $moodswing['HoleNumber'];?></td>
			<td><?php echo $moodswing['Stroke'];?></td>
			<td><?php echo $moodswing['Mood'];?></td>
			<td><?php echo $moodswing['Created'];?></td>
			<td><?php echo $moodswing['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'moodswings', 'action' => 'view', $moodswing['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'moodswings', 'action' => 'edit', $moodswing['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'moodswings', 'action' => 'delete', $moodswing['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $moodswing['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Moodswing', true), array('controller' => 'moodswings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Scorecards');?></h3>
	<?php if (!empty($round['Scorecard'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Round Id'); ?></th>
		<th><?php __('Moodswing Id'); ?></th>
		<th><?php __('Active'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($round['Scorecard'] as $scorecard):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $scorecard['id'];?></td>
			<td><?php echo $scorecard['round_id'];?></td>
			<td><?php echo $scorecard['moodswing_id'];?></td>
			<td><?php echo $scorecard['Active'];?></td>
			<td><?php echo $scorecard['Created'];?></td>
			<td><?php echo $scorecard['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'scorecards', 'action' => 'view', $scorecard['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'scorecards', 'action' => 'edit', $scorecard['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'scorecards', 'action' => 'delete', $scorecard['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $scorecard['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Scorecard', true), array('controller' => 'scorecards', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
