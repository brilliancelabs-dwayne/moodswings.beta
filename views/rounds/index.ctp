<div class="rounds index">
	<h2><?php __('Rounds');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('device_id');?></th>
			<th><?php echo $this->Paginator->sort('course_id');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($rounds as $round):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $round['Round']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($round['Device']['id'], array('controller' => 'devices', 'action' => 'view', $round['Device']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($round['Course']['id'], array('controller' => 'courses', 'action' => 'view', $round['Course']['id'])); ?>
		</td>
		<td><?php echo $round['Round']['Active']; ?>&nbsp;</td>
		<td><?php echo $round['Round']['Created']; ?>&nbsp;</td>
		<td><?php echo $round['Round']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $round['Round']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $round['Round']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $round['Round']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $round['Round']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Round', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Devices', true), array('controller' => 'devices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Device', true), array('controller' => 'devices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Courses', true), array('controller' => 'courses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Course', true), array('controller' => 'courses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Moodswings', true), array('controller' => 'moodswings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Moodswing', true), array('controller' => 'moodswings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Scorecards', true), array('controller' => 'scorecards', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Scorecard', true), array('controller' => 'scorecards', 'action' => 'add')); ?> </li>
	</ul>
</div>