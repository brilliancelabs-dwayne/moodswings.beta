<h3>How did that feel?</h3>
<div class="actions">
	<ul class="moods">
		<li id="mood1"><?php $image1 = $this->Html->image('icons/moods/1.png');
		echo $this->Html->link($image1, $url.'1', array('escape' => false));
		?>
		</li>
		<li id="mood2"><?php $image1 = $this->Html->image('icons/moods/2.png');
		echo $this->Html->link($image1, $url.'2', array('escape' => false));
		?>
		</li>
		<li id="mood3"><?php $image1 = $this->Html->image('icons/moods/3.png');
		echo $this->Html->link($image1, $url.'3', array('escape' => false));
		?>
		</li>
		<li id="mood4"><?php $image1 = $this->Html->image('icons/moods/4.png');
		echo $this->Html->link($image1, $url.'4', array('escape' => false));
		?>
		</li>
	</ul>
</div>