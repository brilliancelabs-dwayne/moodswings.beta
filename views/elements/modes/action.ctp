<h3>What's next?</h3>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link('Continue Hole', array('controller' => 'play', 'action' => 'round', $nextstroke));?></li>
		<li><?php echo $this->Html->link('End Hole', '/play/round/'.@$nexthole);?></li>
		<li><?php echo $this->Html->link('Pause', array('controller' => 'play', 'action' => 'pause', $nextstroke));?></li>
		<li><?php echo $this->Html->link('Exit', array('controller' => 'play', 'action' => 'endgame'));?></li>						
	</ul>
</div>