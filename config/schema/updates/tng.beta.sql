-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tng_beta
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `club_types`
--

DROP TABLE IF EXISTS `club_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `club_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(75) NOT NULL,
  `Description` text,
  `Active` char(1) DEFAULT '1',
  `Created` date DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `club_types`
--

LOCK TABLES `club_types` WRITE;
/*!40000 ALTER TABLE `club_types` DISABLE KEYS */;
INSERT INTO `club_types` VALUES (1,'Wood','Formerly made of wood.  Synonymous with metal or titanium clubs','1','2013-06-08','2013-06-08 22:16:03'),(2,'Iron','Traditional iron clubs','1','2013-06-08','2013-06-08 22:16:28'),(3,'Iron (graphite shaft)','Iron clubs with a graphite shaft','1','2013-06-08','2013-06-08 22:18:04'),(4,'Iron Wedge','Traditional iron wedge club','1','2013-06-08','2013-06-08 22:18:11'),(5,'Iron Wedge (graphite shaft)','Iron wedge with graphite shaft','1','2013-06-08','2013-06-08 22:18:17'),(6,'Putter','Putter of normal length; any face type','1','2013-06-08','2013-06-08 22:19:51'),(7,'Putter (long)','Long and belly putters; any face type','1','2013-06-08','2013-06-08 22:20:12');
/*!40000 ALTER TABLE `club_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clubs`
--

DROP TABLE IF EXISTS `clubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clubs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `Name` varchar(75) NOT NULL,
  `club_type_id` int(11) NOT NULL,
  `Description` text,
  `Active` char(1) DEFAULT '1',
  `Created` date DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `DynKey` (`device_id`,`club_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clubs`
--

LOCK TABLES `clubs` WRITE;
/*!40000 ALTER TABLE `clubs` DISABLE KEYS */;
INSERT INTO `clubs` VALUES (1,1,'Driver',1,'Nike ignite 450 13-degrees','1','2013-06-08','2013-06-08 22:49:24'),(2,1,'3-Wood',1,'Arnold Palmer Axiom M.S. Plus 16-degrees','1','2013-06-08','2013-06-08 22:49:30'),(3,1,'4-iron',2,'Arnold Palmer Axiom M.S. Plus','1','2013-06-08','2013-06-08 22:49:56'),(4,1,'5-iron',2,'Arnold Palmer Axiom M.S. Plus','1','2013-06-08','2013-06-08 22:50:29'),(5,1,'6-iron',2,'Arnold Palmer Axiom M.S. Plus','1','2013-06-08','2013-06-08 22:50:36'),(6,1,'7-iron',2,'Arnold Palmer Axiom M.S. Plus','1','2013-06-08','2013-06-08 22:50:44'),(7,1,'8-iron',2,'Arnold Palmer Axiom M.S. Plus','1','2013-06-08','2013-06-08 22:50:51'),(8,1,'9-iron',2,'Arnold Palmer Axiom M.S. Plus','1','2013-06-08','2013-06-08 22:50:59'),(9,1,'Pitching Wedge',4,'Arnold Palmer Axiom M.S. Plus','1','2013-06-08','2013-06-08 22:51:15'),(10,1,'Sand Wedge',4,'Callaway Warbird 56-degrees','1','2013-06-08','2013-06-08 22:52:09'),(11,1,'Putter',6,'MaxFli Revolution Alignment Putter-3','1','2013-06-08','2013-06-08 22:53:54');
/*!40000 ALTER TABLE `clubs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_holes`
--

DROP TABLE IF EXISTS `course_holes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_holes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `Active` char(1) NOT NULL DEFAULT '1',
  `Color` varchar(25) DEFAULT NULL,
  `HoleNumber` tinyint(4) NOT NULL,
  `Par` tinyint(4) NOT NULL,
  `Yardage` smallint(6) DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_holes`
--

LOCK TABLES `course_holes` WRITE;
/*!40000 ALTER TABLE `course_holes` DISABLE KEYS */;
INSERT INTO `course_holes` VALUES (1,1,'1','White',1,4,429,'2013-06-08','2013-06-08 22:05:00'),(2,1,'1','white',2,4,354,'2013-06-08','2013-06-08 22:05:46'),(3,1,'1','white',3,3,158,'2013-06-08','2013-06-08 22:06:02'),(4,1,'1','white',4,5,461,'2013-06-08','2013-06-08 22:06:15'),(5,1,'1','white',5,4,423,'2013-06-08','2013-06-08 22:06:28'),(6,1,'1','white',6,4,386,'2013-06-08','2013-06-08 22:06:41'),(7,1,'1','white',7,5,502,'2013-06-08','2013-06-08 22:06:52'),(8,1,'1','white',8,3,191,'2013-06-08','2013-06-08 22:07:07'),(9,1,'1','white',9,4,385,'2013-06-08','2013-06-08 22:07:19'),(10,1,'1','white',10,4,434,'2013-06-08','2013-06-08 22:07:36'),(11,1,'1','white',11,4,421,'2013-06-08','2013-06-08 22:07:46'),(12,1,'1','white',12,4,432,'2013-06-08','2013-06-08 22:08:07'),(13,1,'1','white',13,5,480,'2013-06-08','2013-06-08 22:08:20'),(14,1,'1','white',14,3,152,'2013-06-08','2013-06-08 22:08:31'),(15,1,'1','white',15,4,430,'2013-06-08','2013-06-08 22:08:55'),(16,1,'1','white',16,4,457,'2013-06-08','2013-06-08 22:09:15'),(17,1,'1','white',17,3,195,'2013-06-08','2013-06-08 22:09:29'),(18,1,'1','white',18,4,394,'2013-06-08','2013-06-08 22:09:44');
/*!40000 ALTER TABLE `course_holes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `Name` varchar(75) NOT NULL,
  `Description` text,
  `Active` char(1) DEFAULT '1',
  `SubCourseName` varchar(75) DEFAULT NULL,
  `Slope` tinyint(4) DEFAULT NULL,
  `Rating` double(4,1) DEFAULT NULL,
  `ApiCourseId` int(11) DEFAULT NULL,
  `Created` date DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,1,'Bethpage','Bethpage Black Course is a public golf course on Long Island, New York, the most difficult of the five courses at Bethpage State Park. In 2002, the Black Course became the first publicly owned and operated course to host the U.S. Open, which returned in 2009. Bethpage Black hosted The Barclays, the event of the FedEx Cup Playoffs in late August, in 2012 and is scheduled to host again in 2016.','1','Black Course',127,78.1,NULL,'2013-06-08','2013-06-08 21:56:15');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(75) NOT NULL,
  `Description` text,
  `Active` char(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1,'Android1','Android-data-here','1');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moodswings`
--

DROP TABLE IF EXISTS `moodswings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moodswings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `round_id` int(11) NOT NULL,
  `club_id` int(11) NOT NULL,
  `Active` char(1) NOT NULL DEFAULT '1',
  `HoleNumber` tinyint(4) NOT NULL,
  `Stroke` tinyint(4) NOT NULL,
  `Mood` tinyint(4) NOT NULL,
  `Created` date DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `DynKey` (`round_id`,`club_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moodswings`
--

LOCK TABLES `moodswings` WRITE;
/*!40000 ALTER TABLE `moodswings` DISABLE KEYS */;
/*!40000 ALTER TABLE `moodswings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rounds`
--

DROP TABLE IF EXISTS `rounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rounds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `Active` char(1) NOT NULL DEFAULT '1',
  `Created` date DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `DynKey` (`device_id`,`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rounds`
--

LOCK TABLES `rounds` WRITE;
/*!40000 ALTER TABLE `rounds` DISABLE KEYS */;
INSERT INTO `rounds` VALUES (1,1,1,'1','2013-06-08','2013-06-08 22:11:14');
/*!40000 ALTER TABLE `rounds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scorecards`
--

DROP TABLE IF EXISTS `scorecards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scorecards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `round_id` int(11) NOT NULL,
  `moodswing_id` int(11) NOT NULL,
  `Active` char(1) NOT NULL DEFAULT '1',
  `Created` date DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `DynKey` (`round_id`,`moodswing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scorecards`
--

LOCK TABLES `scorecards` WRITE;
/*!40000 ALTER TABLE `scorecards` DISABLE KEYS */;
/*!40000 ALTER TABLE `scorecards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `Email` varchar(75) DEFAULT NULL,
  `username` varchar(75) DEFAULT NULL,
  `password` varchar(75) DEFAULT NULL,
  `FirstName` varchar(75) DEFAULT NULL,
  `MiddleInitial` char(1) DEFAULT NULL,
  `LastName` varchar(75) DEFAULT NULL,
  `Suffix` varchar(15) DEFAULT NULL,
  `Active` char(1) DEFAULT '1',
  `Created` date DEFAULT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Email` (`Email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'dwayneford@gmail.com','dwayne','bed6ab1857dbd7bb40f0fab82d39eada9a5fb7e1','Dwayne','','Ford','Jr.','1','2013-06-08','2013-06-08 23:03:02');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-12 17:32:34
