$(document).ready(function(){
	$('td').click(function(){
		//alert( $(this).parent('tr').children(':first').text() );
	});
	
	//All div CSS is made for index so add that class to form divs	
	$('div.form').addClass('index');


	//Re-label submit buttons
	var butVal = $(':submit').val();
	if(butVal=="Submit"){
		$(':submit').val('Save')
	}

	//Form validation
	$('.required > label').hover(function(){
		window.tmp  = $(this).text();
		$(this).css({
			'background-color':'skyblue',
			'color': 'white',

		});
		$(this).text('Required');

	}, function(){
		$(this).css({
			'background-color':'transparent',
			'color': '#333'
		});		
		$(this).text(tmp);
	});

	//Change the color of the label block for required elements
	$('.required > label').css({'border-right-color': 'skyblue'});


	//Prevent dropdowns from being multiple lists (hate those)
	$('select').removeAttr('multiple');

	
	//Make all labels the same width (equal to the longest)
	//soon to deprecate...use CSS better please
	window.stuff = 175;
	$.each($('label'), function(key,index){

		if($(this).width()<stuff){
			//alert('resizing to '+stuff);

		}else{
			//window.stuff = $(this).width();
		}
	});
	

	//Alter Active boxes, change to selects with Yes and No as the options
	$.each($('label'), function(key,index){
		if($(index).text() == 'Active'){
			var activeID = $(this).attr('for');
			var activeName = $('#'+activeID).attr('name');
			$('#'+activeID).remove();
			$(this).after('<select name="'+
				activeName+'" id="'+
				activeID+'"><option value="1">Yes</option><option value="0">No</option></select>');	
		}
	});

	//Change Created fields to date pickers
	$.each($('.datetime > label'), function(key,index){
		if($(index).text() == 'Created'){

			var activeID = $(this).attr('for');

			$(this).parent().html('<label for="date">Created</label><input type="input" class="datepicker" />');

		}
	});

	//use jquery-ui to create a datepick for the date input field
	$('body').on('click', '.datepicker', function(){
		$(this).dateinput();	
		
	});

	//Change submits to Saves
	$('.form > div.submit').prepend('<label for="save">&nbsp;</label>');	

}); //end of file
