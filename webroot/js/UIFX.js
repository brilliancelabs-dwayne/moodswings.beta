$(document).ready(function(){

   var base = window.base;

   var UIFX = {

      _initialize:function() {  
         this.events();
         this.animate_points();
         this.remove_cake_dates();
         this.remove_cake_timestamps();
         this.load_data();
         this._tmp_styles();
         console.log("[EVENT]:UI js loaded");

       },      
      _tmp_styles: function(){
    	  $(".loading").css({
    		  "padding": "2px 10px",

    	  })
    	  
		//Fade out flash messages
		$('#flashMessage').delay(7000).fadeOut(3000).css({
			
		});
      },
      ajax_url: base+"exec/auto_label/",
            
      animate_points: function(){
          //Get default width
          var BarWidth = $(".slider-bar").css("width");
          //Quickly reset to zero
          $(".slider-bar").css("width",0);

          //Animate back to default value
          $(".slider-bar").animate({
             "width": BarWidth,
          },5000,function(){
             console.log('animation complete');
          })
      },
      
      auto_label: function(
                     eclass, //eclass is the model name, i.e. "Person"
                     id //the ID that should be retrieved
                  ){

         //uid is an ID from the passed model
         var url = this.ajax_url+eclass+'/'+id;

         //Get the DOM element id of the passed class			
         var eid = '#'+$('.'+eclass).attr('id');

      },          

      dropdown_to_text: function(){

         //Big Lists -- Manually added list that will be handled by this function
         var BigBoys = ["[drug_id]","[condition_id]"];
         
         $self = this;
         
         $.each($('select'), function(k,v){
            
            var self = this;
            
            $.each(BigBoys, function(index,value){
               //See if this field is in the big boy list
               //var test = $(self).attr('name').replace(value, 'test');  
               
               test = $(self).attr('name').replace(value,'test');

               if(test != $(self).attr('name')){
                  
                  var vname = $(self).attr('name');

                  //Get the model name by parsing from the Add or Edit form ID value
                  var Model = $("form").attr("id").replace("AddForm","").replace("EditForm","");

                  //Using the model strip the field name down to the requested data element
                     //use this name as the id that will be used in the ajax request
                  var ajax_id = vname.replace("data["+Model+"]","")
                              .replace("[","")
                              .replace("]","");

                  //Get the text that will be used in a query string
                  var qstring = $(self).val();

                  var vid = $(self).attr('id');

                  var vclass = $(self).attr('class');

                  $(self).parent('div').children('label').after('<input id="'+vid+'" class="autocomplete" name="'+vname+'"  />');

                  if(typeof vclass != "undefined"){
                     $('#'+vid).addClass(vclass);
                  }

                  //Hide the loading span on the initial load
                  $(self).next('.loading').hide();

                  $(self).remove();

                  //If possible change the default qstring to a number
                     //since it will default to a number after dropdown conversion
                  qstring = parseInt(qstring);

                  //Make sure the input element is a string
                  var dataSrc = "/exec/get_data/"+ajax_id+"/"+qstring;
                  if(typeof ajax_id == "string" && typeof qstring == "string"){
                     console.log("Good Data SRC");
                     //Load Autocomplete function
                     $('.autocomplete').autocomplete({
                        minLength: 0,
                        search: "",
                        source: dataSrc
                     }); 
                  }else{
                     //console.log("Bad Data SRC")
                     //console.log("wrong data type")
                  }
               }else{
                  //console.log(qstring+" is NOT A BIG BOY");
               }            
            }); //end Big Boy test
            
         });
      },
              
      dropdown_to_text_this: function(x){
         var self = x;
         
         var vname = $(x).attr('name');

         //Get the model name by parsing from the Add or Edit form ID value
         var Model = $("form").attr("id").replace("AddForm","").replace("EditForm","");

         //Using the model strip the field name down to the requested data element
            //use this name as the id that will be used in the ajax request
         var ajax_id = vname.replace("data["+Model+"]","")
                     .replace("[","")
                     .replace("]","");

         //Get the text that will be used in a query string
         var qstring = $(self).val();

         var vid = $(self).attr('id');

         var vclass = $(self).attr('class');

         //$(self).parent('div').append('<input id="'+vid+'" class="autocomplete" name="'+vname+'"  />');

         if(typeof vclass != "undefined"){
            $('#'+vid).addClass(vclass);
         }

         $(self).next('.loading').hide();

         //Make sure the input element is a string
         var dataSrc = "/exec/get_data/"+ajax_id+"/"+qstring;
         if(typeof ajax_id == "string" && typeof qstring == "string"){
            //console.log("Good Data SRC");
            //Load Autocomplete function
            $('.autocomplete').autocomplete({
               minLength: 0,
               search: "",
               source: dataSrc
            }); 
         }else{
            //bad dataSrc
         }
      },
      
      events: function(){
         var $self = this;   
         
         /*Make all autocomplete fields into text boxes*/
         $('.autocomplete', function(){
            console.log("autorun fx");
            $self.dropdown_to_text();

         })  

         $('body').on('keyup', '.autocomplete', function(){
            $self.dropdown_to_text_this(this);
            //console.log("keyup fx");
         })
         
         //when selects are changed run the load_data function
         $("select").change(function(){
            $self.load_data();

         })

         //use jquery-ui to create a datepick for the date input field
         $('body').on('click', '.datepicker', function(){
            $(this).datepicker();	

         });

         //set up the jquery UI datepicker function
          $(function() {
            $( ".datepicker" ).datepicker();

          }); 
             
      },

      find_autocomplete_data: function(x){
         
         var url = base+"exec/get_data/"+x;
         
         $.ajax({
            "url": url,
            "error": function(){
               console.log("Error");
            }
         }).done(function(data){
            console.log(data);
            
         });
      },      
              
      find_index_data: function(){
 
         if($("form").length){

            $.each( $("select :selected"), function(key,option){

               //The Model is the Form ID so grab that
               var Model = $("form").attr("id")
                       .replace("AddForm", "")
                       .replace("EditForm","")

               var Field = 
                    $(option).parent().attr('name')
                     .replace("data["+Model+"]", "")
                     .replace("[","")
                     .replace("]","")

               //Fields that will not be included in the function     
               var BlockedFields = [
                  "Active", "Status", "Refill"
               ]

               //if the field still has brackets it's a date field      
               var endBracket = /\]$/;

               if(endBracket.test(Field)){
                  BlockedFields.push(Field);

               }

               var Id = $(this).val();

               var thisID = $(option).parent().attr('id');

               if($.inArray(Field,BlockedFields) == -1){

                  var url = "/exec/auto_label/"+Field+"/"+Id;

                  $.ajax({

                     url: url,

                     beforeSend: function(){

                      if($('#loading_'+thisID).length < 1){

                        $("#"+thisID)
                            .parent("div")
                                .append('<span class="input-label loading" id="loading_'+thisID+'">Loading...</span>')

                      }else{
                         $('#loading_'+thisID)
                            .text("Loading...");
                      }

                     },

                     statusCode: {
                       500: function(){
                           console.log("500 Error");
                       },
                       403: function(){
                           console.log("403 Error");

                           //not authorized...take to login
                           window.location = "/users/login";

                       },
                     }
                  })           
                  .fail(function(){
                     $('#loading_'+thisID).empty();
                     console.log("Error occurred");

                  })
                  .done(function(data){
                     $('#loading_'+thisID).text(data);
                  })

               }else{

                //console.log($.inArray(Field,BlockedFields));

                //console.log(Field+" was blocked");
               }

            });

         }
      },
         
      get_data: function(){
         
         $self = this;
         
         var url = base+"exec/get_data/condition_id";
         
         var get_data = $.ajax({
            "url": url
            
         })
         
         get_data.fail(function(){
            console.log("Failed to get data");
            
         })
         
         get_data.done(function(data){
    
            jdata = $.parseJSON(data);
            
            //console.log(typeof jdata);
            
            return ["hi","there"];
         })
         
         
         //return ["hi","there"]
         
         

      },     
              
      load_data: function(){
          //Find data for the requested index
          this.find_index_data();
      },              

      remove_cake_dates: function(){
         //Change date fields to date pickers
          $.each($('.date > label'), function(key,index){

             var fname = $(index).text();

             var activeID = $(this).attr('for');

             var activeName = $('#'+activeID).attr('name')
                     //.replace("[month]","")

             if(fname!="Created"){
                //$(this).parent().remove();

                $(this).parent().html(

                   '<label for="date">'

                   +fname

                   +'</label><input name="'

                   +activeName

                   +'" type="input" class="datepicker" id="'

                   +activeID

                   +'" />'

                );


             }else{
                $(this).parent().remove();

             }
          });
      },

      remove_cake_timestamps: function(){
          $.each($('.datetime > label'), function(key,index){

             var fname = $(index).text();

             var activeID = $(this).attr('for');

             var activeName = $('#'+activeID).attr('name');

             if(fname!="Updated"){

                $(this).parent().html('<label for="date">'+

                fname+
                '</label><input name="'+

                activeName+
                '" type="input" class="datepicker" id="'+

                activeID+'" />');

             }else{
                $(this).parent().remove();		
             }
          });

      },

      submits_to_saves: function(){
          //Change submits to Saves
          $('.form > div.submit').prepend('<label for="save">&nbsp;</label>');	
      },
     
   }; //end of UIFX

   UIFX._initialize();

});