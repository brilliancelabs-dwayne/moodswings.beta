$(document).ready(function(){
	$('td').click(function(){
		//alert( $(this).parent('tr').children(':first').text() );
	});
	
	//All div CSS is made for index so add that class to form divs	
	$('div.form').addClass('index');

	//Re-label submit buttons
	var butVal = $(':submit').val();
	if(butVal=="Submit"){
		$(':submit').val('Save')
	}

	//Form validation
	$('.required > label').hover(function(){
		window.tmp  = $(this).text();
		$(this).css({
			'background-color':'skyblue',
			'color': 'white',

		});
		$(this).text('Required');

	}, function(){
		$(this).css({
			'background-color':'transparent',
			'color': '#333'
		});		
		$(this).text(tmp);
	});

	//Change the color of the label block for required elements
	$('.required > label').css({'border-right-color': 'skyblue'});


	//Prevent dropdowns from being multiple lists (hate those)
	$('select').removeAttr('multiple');

	
	//Make all labels the same width (equal to the longest)
	//soon to deprecate...use CSS better please
	window.stuff = 175;
	$.each($('label'), function(key,index){

		if($(this).width()<stuff){
			//alert('resizing to '+stuff);

		}else{
			//window.stuff = $(this).width();
		}
	});

	//Change date fields to date pickers
	$.each($('.date > label'), function(key,index){
		
		var fname = $(index).text();
		
		var activeID = $(this).attr('for');
		
		var activeName = $('#'+activeID).attr('name');
		
		$(this).parent().html('<label for="date">'+
		fname+
		'</label><input name="'+
		activeName+
		'" type="input" class="datepicker" />');

	});

	$.each($('.datetime > label'), function(key,index){
		
		var fname = $(index).text();
		
		var activeID = $(this).attr('for');
		
		var activeName = $('#'+activeID).attr('name');
		
		if(fname!="Updated"){
			$(this).parent().html('<label for="date">'+
			fname+
			'</label><input name="'+
			activeName+
			'" type="input" class="datepicker" />');
		}else{
			$(this).parent().remove();		
		}
	});


	//use jquery-ui to create a datepick for the date input field
	$('body').on('click', '.datepicker', function(){
		$(this).dateinput();	
		
	});

	//Change submits to Saves
	$('.form > div.submit').prepend('<label for="save">&nbsp;</label>');	

}); //end of file
