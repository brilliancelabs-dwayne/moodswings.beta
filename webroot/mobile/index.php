<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>
        </title>
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.1/jquery.mobile-1.1.1.min.css" />
        <link rel="stylesheet" href="my.css" />
        <link rel="stylesheet" href="extra.css" />
        <style>
            /* App custom styles */
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
        </script>
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.1/jquery.mobile-1.1.1.min.js">
        </script>
        <script src="my.js"></script>
        <script src="extra.js">
        </script>
    </head>
    <body>
            <!-- Splash -->
        <div data-role="page" id="page1">
            <div data-role="content">
				<div id="splash-img">
				<a data-role="button" data-theme="" id="splash-link" href="#page2">
				<img 
				src="https://s3.amazonaws.com/assets.codiqa.com/8Ud9xOFaSUml8JV7x6DC_talkngolf.png">
				
				</a>
				</div>
            </div>
        </div>
        <!-- Entry -->
        <div data-role="page" id="page2">
            <div data-role="content">
                <h2>
                    Mood Swings Golf
                </h2>
                <h4>
                    FREE Edition
                </h4>
                <div class="ui-grid-a">
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="e" href="#page3">
                            Play
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="a" href="#page1">
                            Setup
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ChooseCourse -->
        <div data-role="page" id="page3">
            <div data-role="content">
                <h2>
                    Choose Course
                </h2>
                <div class="ui-grid-a">
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page4">
                            Black
                        </a>
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                </div>
            </div>
        </div>
        <!-- ChooseStart -->
        <div data-role="page" id="page4">
            <div data-role="content">
                <h2>
                    Choose Starting Hole
                </h2>
                <div class="ui-grid-a">
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page5">
                            1
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="b" href="#page5">
                            10
                        </a>
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                </div>
            </div>
        </div>
        <!-- Play -->
        <div data-role="page" id="page5">
            <div data-role="content">
                <h2>
                    Choose Club
                </h2>
                <h4>
                    Hole # | Stroke #
                </h4>
                <div class="ui-grid-a">
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page7">
                            Driver
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="b" href="#page7">
                            3-Wood
                        </a>
                    </div>
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page7">
                            4-iron
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="b" href="#page7">
                            4-Hybrid
                        </a>
                    </div>
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page7">
                            5-iron
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="b" href="#page7">
                            6-iron
                        </a>
                    </div>
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page7">
                            7-iron
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="b" href="#page7">
                            8-iron
                        </a>
                    </div>
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page7">
                            9-iron
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="b" href="#page7">
                            PW
                        </a>
                    </div>
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page7">
                            SW
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="b" href="#page7">
                            Putter
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ChooseMood -->
        <div data-role="page" id="page7">
            <div data-role="content">
                <h2>
                    Choose Mood
                </h2>
                <h5>
                    How did that feel?
                </h5>
                <div class="ui-grid-c">
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="c" href="#page8">
                            1
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="b" href="#page8">
                            2
                        </a>
                    </div>
                    <div class="ui-block-c">
                        <a data-role="button" data-theme="b" href="#page8">
                            3
                        </a>
                    </div>
                    <div class="ui-block-d">
                        <a data-role="button" data-theme="e" href="#page8">
                            4
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ChooseAction -->
        <div data-role="page" id="page8">
            <div data-role="content">
                <h2>
                    What's Next?
                </h2>
                <div class="ui-grid-a">
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page5">
                            Next Stroke
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" data-theme="b" href="#page5">
                            End Hole
                        </a>
                    </div>
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="d" href="#page9">
                            Pause
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" href="#page10">
                            Exit
                        </a>
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                    <div class="ui-block-a">
                    </div>
                    <div class="ui-block-b">
                    </div>
                </div>
            </div>
        </div>
        <!-- Pause -->
        <div data-role="page" id="page9">
            <div data-role="content">
                <h2>
                    Tracking Paused
                </h2>
                <h5>
                    Your data has been saved
                </h5>
                <div class="ui-grid-a">
                    <div class="ui-block-a">
                        <a data-role="button" data-theme="b" href="#page5">
                            Continue
                        </a>
                    </div>
                    <div class="ui-block-b">
                        <a data-role="button" href="#page1">
                            Exit
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Thanks -->
        <div data-role="page" id="page10">
            <div data-role="content">
                <h2>
                    Mood Swings Beta
                </h2>
                <h4>
                    Thanks for tracking!
                </h4>
            </div>
        </div>
        <script>
            //App custom javascript
        </script>
    </body>
</html>