<?php
class MoodswingsControllerAddendum extends MoodswingsController {
	
	public function play_add(){
		$post =& $this->data['Moodswing'];
		$rmood =@ $this->params['pass'][0];
		$mood_or_stroke =@ $this->params['pass'][1];

		//Request mood if the URL is called
		if($rmood == "getmood"):
	
		$for_stroke = $mood_or_stroke;
		
		if(!$for_stroke):
			$for_stroke = $this->Session->read('Play.Current.Stroke');
			
			
			$this->set('get_mood', 1);
		endif;
		
		//Check session for course id
		if($this->Session->check('Play.Course.id')):
		
			//unnecessary b/c session is used in the view
			$this_course = $this->Session->read('Play.Course.id');
			
			//Load the golfers bag
			$clubs = $this->Moodswing->Club->find('list', array(
					'fields' => array(
						'id', 'Name'	
					),
					'conditions' => array(
						'Club.device_id' => $this->Session->read('Auth.User.device_id')		
					)
			));
			
			//Re-route the golfer to the club screen if there are no clubs
			if(empty($clubs)):
				$this->Session->setFlash("You must load clubs before you play");
				$this->redirect(array(
					'controller' => 'clubs',
					'action' => 'add'		
				));
			else:
				$this->set('clubs', $clubs);
			endif;
			
			
			//See if new information has been entered
			if($post):
				$hole = $this->Session->read('Play.Current.Hole');
				$stroke = $this->Session->read('Play.Current.Stroke');
				
				//Track the moodswing data in a session
				$this->Session->write('Play.Moodswings.'.$hole.'.'.$stroke, 0);
				
				
				
			endif;
			
			
			
		//Then check form data
		elseif(isset($post['Course'])):
			
			$this_course = $post['Course'];
			//Initialize the play session
			$this->Session->write('Play.Golfer.Name', 'Jim McQueery');
			$this->Session->write('Play.Course.Temperature', '78.6C');
			$this->Session->write('Play.Course.id', $this_course);
		
			//Current play data
			$this->Session->write('Play.Current.Hole', @$post['StartHole']);
			$this->Session->write('Play.Current.Stroke', 1);
			$this->Session->write('Play.Moodswings', array());
			
		//Request that the user choose a course
		else:
			$this->loadModel('Course');
			$courses[0] = "Pick One";
			$list = $this->Course->find('list', array('fields' => array('id', 'CourseName')));
			
			$courses = array_merge($courses,$list);
			$this->set('courses', $courses);	
				
		endif;
		

		
	}
}