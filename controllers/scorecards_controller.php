<?php
class ScorecardsController extends AppController {

	var $name = 'Scorecards';

	function index() {
		$this->Scorecard->recursive = 0;
		$this->set('scorecards', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid scorecard', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('scorecard', $this->Scorecard->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Scorecard->create();
			if ($this->Scorecard->save($this->data)) {
				$this->Session->setFlash(__('The scorecard has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scorecard could not be saved. Please, try again.', true));
			}
		}
		$rounds = $this->Scorecard->Round->find('list');
		$moodswings = $this->Scorecard->Moodswing->find('list');
		$this->set(compact('rounds', 'moodswings'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid scorecard', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Scorecard->save($this->data)) {
				$this->Session->setFlash(__('The scorecard has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The scorecard could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Scorecard->read(null, $id);
		}
		$rounds = $this->Scorecard->Round->find('list');
		$moodswings = $this->Scorecard->Moodswing->find('list');
		$this->set(compact('rounds', 'moodswings'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for scorecard', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Scorecard->delete($id)) {
			$this->Session->setFlash(__('Scorecard deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Scorecard was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
