<?php
class MoodswingsController extends AppController {

	var $name = 'Moodswings';
	public function play(){
		
	}
	function index() {
		$this->Moodswing->recursive = 0;
		$this->set('moodswings', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid moodswing', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('moodswing', $this->Moodswing->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Moodswing->create();
			if ($this->Moodswing->save($this->data)) {
				$this->Session->setFlash(__('The moodswing has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The moodswing could not be saved. Please, try again.', true));
			}
		}
		$rounds = $this->Moodswing->Round->find('list');
		$clubs = $this->Moodswing->Club->find('list');
		$this->set(compact('rounds', 'clubs'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid moodswing', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Moodswing->save($this->data)) {
				$this->Session->setFlash(__('The moodswing has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The moodswing could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Moodswing->read(null, $id);
		}
		$rounds = $this->Moodswing->Round->find('list');
		$clubs = $this->Moodswing->Club->find('list');
		$this->set(compact('rounds', 'clubs'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for moodswing', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Moodswing->delete($id)) {
			$this->Session->setFlash(__('Moodswing deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Moodswing was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
