<?php
class CourseHolesController extends AppController {

	var $name = 'CourseHoles';

	function index() {
		$this->CourseHole->recursive = 0;
		$this->set('courseHoles', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid course hole', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('courseHole', $this->CourseHole->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->CourseHole->create();
			if ($this->CourseHole->save($this->data)) {
				$this->Session->setFlash(__('The course hole has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The course hole could not be saved. Please, try again.', true));
			}
		}
		$courses = $this->CourseHole->Course->find('list');
		$this->set(compact('courses'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid course hole', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->CourseHole->save($this->data)) {
				$this->Session->setFlash(__('The course hole has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The course hole could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->CourseHole->read(null, $id);
		}
		$courses = $this->CourseHole->Course->find('list');
		$this->set(compact('courses'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for course hole', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->CourseHole->delete($id)) {
			$this->Session->setFlash(__('Course hole deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Course hole was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
