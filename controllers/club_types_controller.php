<?php
class ClubTypesController extends AppController {

	var $name = 'ClubTypes';

	function index() {
		$this->ClubType->recursive = 0;
		$this->set('clubTypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid club type', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('clubType', $this->ClubType->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ClubType->create();
			if ($this->ClubType->save($this->data)) {
				$this->Session->setFlash(__('The club type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The club type could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid club type', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ClubType->save($this->data)) {
				$this->Session->setFlash(__('The club type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The club type could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ClubType->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for club type', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ClubType->delete($id)) {
			$this->Session->setFlash(__('Club type deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Club type was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
