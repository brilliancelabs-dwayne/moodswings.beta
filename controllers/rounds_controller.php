<?php
class RoundsController extends AppController {

	var $name = 'Rounds';

	function index() {
		$this->Round->recursive = 0;
		$this->set('rounds', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid round', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('round', $this->Round->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Round->create();
			if ($this->Round->save($this->data)) {
				$this->Session->setFlash(__('The round has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The round could not be saved. Please, try again.', true));
			}
		}
		$devices = $this->Round->Device->find('list');
		$courses = $this->Round->Course->find('list');
		$this->set(compact('devices', 'courses'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid round', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Round->save($this->data)) {
				$this->Session->setFlash(__('The round has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The round could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Round->read(null, $id);
		}
		$devices = $this->Round->Device->find('list');
		$courses = $this->Round->Course->find('list');
		$this->set(compact('devices', 'courses'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for round', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Round->delete($id)) {
			$this->Session->setFlash(__('Round deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Round was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
