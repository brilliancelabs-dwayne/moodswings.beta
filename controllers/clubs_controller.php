<?php
class ClubsController extends AppController {

	var $name = 'Clubs';

	function index() {
		$this->Club->recursive = 0;
		$this->set('clubs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid club', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('club', $this->Club->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Club->create();
			if ($this->Club->save($this->data)) {
				$this->Session->setFlash(__('The club has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The club could not be saved. Please, try again.', true));
			}
		}
		$devices = $this->Club->Device->find('list');
		$clubTypes = $this->Club->ClubType->find('list');
		$this->set(compact('devices', 'clubTypes'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid club', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Club->save($this->data)) {
				$this->Session->setFlash(__('The club has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The club could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Club->read(null, $id);
		}
		$devices = $this->Club->Device->find('list');
		$clubTypes = $this->Club->ClubType->find('list');
		$this->set(compact('devices', 'clubTypes'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for club', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Club->delete($id)) {
			$this->Session->setFlash(__('Club deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Club was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
