<?php
class PlayController extends AppController {

	public $name = 'Play';
	
	public $uses = array('Course', 'Moodswing', 'Round');
	
	private function init_round($hole){
		$status = $this->Session->read('Play.Round.Status');

		if(!$status):
			//Set the value and restart this method
			$this->Session->write('Play.Round.Status', 'init');
		
			$this->init_round($hole);
			
		else:
			switch($status):
			
				case "init":
					//set the init hole
					$this->Session->write('Play.Round.'.$hole, array());
					
					//Change the status to Active
					if($this->Session->check('Play.Round.'.$hole)):
						$this->Session->write('Play.Round.Status', 'Active');
						$this->init_round($hole);

					else:
						//re-run
						$this->init_round($hole);
					
					endif;
					
					break;
					
				case "Active":
					debug("Active");
					break;
		
				default:
					//do nada
					
			endswitch;
		endif;
		
		
		$this->redirect('start');
	}	
	private function verify_course(){
		if(!$this->Session->check('Play.Course.id')):
			//Redirect to the course chooser
			$this->redirect('choose');
		else:
			//Do something else
		endif;		
	}
	private function verify_round($hole){
		if(!$hole):
			$this->redirect('start');
		
		endif;
		/*
		//Check to see if the round is active
		if($this->Session->read('Play.Round.Status') == "Active"):
			
			echo "Active"; 
			//debug($this->Session->read('Play.Round'));

		else:
		
			echo "hole ".$hole;
			//init the round
			$this->init_round($hole);
		
			//verify again
			debug("reverify", "red", "white");
			
		endif;
		*/
	}
	private function verify_session(){
		if(!$this->Session->check('Play')):
			//Redirect to the Play Session loader
			$this->redirect('load');
		else:
		//Do something else
			
		endif;
	}


	public function index($hole = null, $stroke = null, $club = null, $mood = null, $action = null) {

		/**
		 * VERIFICATION PHASES
		 */
		//Make sure everything is in order before playing
		$this->verify_session();
		
		//Verify course
		$this->verify_course();

		//Verify round
		$round = $this->Session->read('Play.Round');
		$shole =@ array_keys($round);
		$shole =& $shole[0];
		$this->verify_round($shole);
		
		//If this is reached then all verification passed
		//Begin play tracking
		
		//Get the players clubs
		$clubs = $this->getAll('Club', 'device_id', $this->Session->read('Auth.User.device_id'), -4);

		//If hole is not present then reload this page with the session-loaded hole
		$pass =@ $this->params['pass'];
		$play = $this->Session->read('Play');
		
		if(!$hole):
			$this->redirect(array('action' => 'round', $shole, 1));
			$mode = "hole";
			
		elseif(!$stroke):
			//Should not happen sense a 1 is automatically appended
			$this->redirect(array('action' => 'round', $shole, 1));
			$mode = "stroke";
			
		elseif(!$club):
			$mode = "club";
			$this->set(compact('clubs'));
			
		elseif(!$mood):
			$mode = "mood";

		//All params are loaded
		elseif(!$action):
			$mode = "action";
		
			//build nextstroke link
			$nextstroke = $hole."/"; //prefix the next stroke with the current hole
			$nextstroke.=$stroke+1;
			
			//build nexthole link
			$nexthole = $hole+1;
			$nexthole.="/1"; //add 1st stroke to the next hole
			
			//Save the stroke data to Session
			$_SESSION['Play']['Round'][$hole][$stroke][$clubs[$club]['id']] = $mood;
			//$this->Session->write('Play.Round.'.$hole, array($stroke => $mood));
			
			//Save the stroke data to the database
			
			
			$this->set(compact('nextstroke','nexthole'));
			
		else:
			//done
			debug('do something');
		endif;
		
		
		//Get the MoodSwing Logs
		$round = $this->Session->read('Play.Round');
		
		//debug($play);
		
		$shot_log =@ $round[$hole];
		
		
		//Send variables to View
		$this->set(compact('mode', 'shot_log', 'hole', 'stroke', 'club', 'clubs', 'mood'));
		
		
		
		
	}
	
	public function choose($course_id = null){
		$this->verify_session();
		
		//If requested check to see if course exists
		$device_id = $this->Session->read('Auth.User.device_id');

		if($course_id):
			$this_course = $this->Course->find('first', array(
				'recursive' => -1,
				'conditions' => array(
					'Course.id' => $course_id,
					'Course.device_id' => $device_id
				)
			));		
		
			$course = $this_course['Course'];
			$course_name = $course['Name']." ".$course['SubCourseName'];
			$course_id = $course['id'];
			
			//Set the course name and ID to the Play Session
			$this->Session->write(
				'Play.Course.id', $course_id,
				'Play.Course.Name', $course_name
			);
					
			//redirect to Play
			$this->redirect('index');
	
		else:
			//Send a list of this devices courses to view
			$courses = $this->getAll('Course', 'device_id', $this->Session->read('Auth.User.device_id'), -4);
			
			//Format with CourseName
				//Model gives error when creating concatenated CourseName
			if(!empty($courses)):
				foreach($courses as $c):
					$course_list[$c['id']] = $c['Name']."  ".$c['SubCourseName'];
			
				endforeach;
				
				//Send newly formatted course list to view
				$courses = $course_list;
				$this->set(compact('courses'));		
			else:
				//do not send courses array to view
			endif;
		endif;
	}
	public function load() {
		//Never verify session here since the redirect would loop
		//$this->verify_session();
		
		//Basic view setup
		$this->autoRender = false;
		echo "Loading...";		

		//Check to see if there are any courses for this device
		$courses = $this->Course->find('list', array(
			'recursive' => '-1',
			'conditions' => array(
				'Course.device_id' => $this->Session->read('Auth.User.device_id')		
			)	
		));	
		
		//If there are no courses set up by this user then require course adding
		if(empty($courses)):
			$this->redirect(array(
				'controller' => 'courses',
				'action' => 'add'		
			));
		endif;
		
		//Check if the Play Session has been loaded
		$play = $this->Session->read('Play');
		
		//Continue loading only if the Play Session has been initialized
		if(empty($play)):
			//Intialize Play Session if not loaded
			$name = $this->Session->read('Auth.User.FirstName');
			$this->Session->write('Play.Golfer.Name', $name);
			$this->Session->write('Play.Course.Temperature', '78.6C'); //get temp from API
			$this->load();
		
		//if the Play Session is loaded check to see if a course has been selected
		elseif(!$this->Session->check('Play.Course.id')):
			$this->Session->setFlash('Please choose a course');
			$this->redirect('choose');
		
			
		//Everything is in order, proceed
		else:
			$this->redirect('index');
		
		endif;
		
		//Reload if the course has not yet been loaded into Session
		#$this->verify_course();
		
/**		

		
		//Check session for course id
		if($this->Session->check('Play.Course.id')):
		
			//unnecessary b/c session is used in the view
			$this_course = $this->Session->read('Play.Course.id');
			
			//Initialize the play session
			$name = $this->Session->read('Auth.User.FirstName');
			$this->Session->write('Play.Golfer.Name', $name);
			$this->Session->write('Play.Course.Temperature', '78.6C');
			$this->Session->write('Play.Course.id', $this_course);
			
			//Current play data
			$this->Session->write('Play.Current.Hole', @$post['StartHole']);
			$this->Session->write('Play.Current.Stroke', 1);
			$this->Session->write('Play.Moodswings', array());
			
		elseif(!empty($courses)):
			debug($courses);
		
		endif;
		
		$play = $this->Session->read('Play');
		if(!empty($play) && 1):
		
			//$this->redirect(1);
		
		endif;
**/		
		
	}
	public function pause($nextstroke = null){
		//Save current round data
		
		
		//Pick back up with the next stroke
		
	}
	public function endgame(){
		//Save current round data
		
	}
	
	public function start($hole = null){
		if($hole && !$this->Session->check('Play.Round.'.$hole)):
			$play = $this->Session->read('Play');
			
			//Add starting hole
			$this->Session->write('Play.Round', array());
			$this->Session->write('Play.Round.'.$hole, array());
			$this->start($hole);
		elseif($hole && $this->Session->check('Play.Round.'.$hole)):
			//Go back to load
			$this->load();
		
		else:
			//choose a starting hole
		endif;
		
		/*
		$this->verify_round($hole);
		
		debug($this->Session->read('Play.Round'));
		
		if($hole):
			$this->init_round($hole);
		
		elseif($this->Session->check('Play.Round')):
		
			$this->verify_round($hole);
		
		else:
			//do nothing so the golfer may choose a starting hole
		endif;
		*/
	}	
}
?>