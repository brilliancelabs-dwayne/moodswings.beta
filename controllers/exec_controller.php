<?php
class ExecController extends AppController {
   
   public $name = "Exec";

   //Uses no db tables
   public $uses = array(
   );
 
   public function index(){
   		$this->autoRender = false;
   		
   		
   }
   
   //default layout is ajax
   public function beforeFilter(){
      $this->layout = "ajax";
   }
   
   //clear a particular session object
   public function clear ($what = null){
      if($what):
         unset($_SESSION[$what]);
         $this->redirect('/');

      endif;
   }

   //Placeholder for backup db method - DNW
   public function backupdb(){
       print('loading new db...May take a while!');
   }

   //Placeholder for remote db sync method - DNW
   public function syncdb(){
       $this->autoRender = false;

       //backup db to file
       $this->backupdb();

       //sync the db            
       $this->Devsync->db();

   }

   //Toggles settings inside the Session Feature object
   		//$_SESSION['Feature'][$key]
   public function toggle($feature = null, $value = null){
      $this->autoRender = false;
      if($feature):
         $key = 'Feature.'.$feature;
         $now = $this->Session->read($key);


         if(!$now && !$value):
            $this->Session->write($key,1);

         elseif(!$now && $value):
            $this->Session->write($key,$value);

         elseif($now && !$value):
            $this->Session->write($key,0);

         elseif($now && $value):
            $this->Session->write($key,$value);

         else:
            $this->Session->write($key,1);
         endif;
      endif;

      $this->redirect('/');
   }     

   //Automatically finds the name of a numbered index then provides value for AJAX loading
   public function auto_label($field = null, $id = null){
      $this->autoRender = false;
      $this->layout = 'ajax';

      if($field):

         $model = str_replace("_id", "", $field);

         $model = inflector::classify($model);

         $this->loadModel($model);

         $find = $this->$model->find('first', array('recursive' => -1, 'conditions' => array(
             $model.".id" => $id
         )));

         $set = $find[$model];

         //Special return policies by model
         switch($model):                    
            /*return a custom label based on the requested model              
            case "SpecialCase":
         	
         	break;
         	*/

            default:
               if(isset($set['Name'])):
                  $return = "Name";

               elseif(isset($set['person_id'])):
                  $pid = "person_id";
                  $name = $this->get('Person', 'Name', 'id', $set[$pid], -1);

               endif;

         endswitch;

           
         //Make a special return
         if(isset($return)):
            echo $set[$return];

         elseif(isset($name)):
            echo $name;

         else:
            //echo "No result";

         endif;



      endif;
   }

   //Placeholder to trigger PHP effect from front end JS code
   public function ui($function = null, $param = null){

      $this->autoRender = false;

      switch($function):

         case "flash":          
            if($param):     
               echo $param;
            endif;
            break;

      endswitch;

   }

   /**UNUSED
   public function ajget($set = null){
      $this->autoRender = false;
      switch($set):
         case "people":
            $this->loadModel('Person');
            $find = $this->Person->find('all', array(
                'recursive' => -1,
                'fields' => 'Person.Name',
            ));
            foreach($find as $f):
               $names[] = $f['Person']['Name'];
            endforeach; 

            $names = json_encode($names);
            echo $names;

            break;


      endswitch;


   }
	
   public function get_data($field = null, $qstring = ""){
      //field for example might be drug_id
      $this->autoRender = false;
      
      $this->Session->write('Feature.layout_override', 'ajax');
      
      //in this function the _id is stripped and the word is formatted to retrieve the model name
      if($field):
         
         $model = str_replace("_id", "", $field);

         $model = inflector::classify($model);     
         
         $this->loadModel($model);
         
         $list = $this->$model->find('list', array(
              'fields' => array(
                   'id', 
                   'Name'
              ),
              'limit' => 10,
              'conditions' => array(
                   'Name LIKE' => '%'.$qstring.'%'
              )
              
         ));
         //Configure::write('debug', 2);
         //debug($list);
         foreach($list as $k=>$v):
            $new_list[] = array("value"=>$k,"label"=>$v);
         
         endforeach;
         $json_data = json_encode($new_list, TRUE);
         
         echo $json_data;
         //echo $json_data;
         
      endif;
   }

   public function afterFilter() {
      unset($_SESSION['Feature']);
      
      
   }
   **/
   
}//class dismissed
